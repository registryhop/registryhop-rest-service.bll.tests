﻿using NUnit.Framework;
using RegistryHopRestService.BLL.Application.Account.Confirm.Model;

namespace RegistryHopRestService.BLL.Tests.Application.Accounts.Confirm.Model
{
    [TestFixture]
    public class AccountConfirmRequestTest
    {
        [Test]
        public void GivenAConfirmationCodeWhenInstantiatingANewAccountConfirmRequestThenExpectCorrectObject()
        {
            var accountConfirmRequest = new AccountConfirmRequest();
            var expectedConfirmationCode = "Test1234";
            accountConfirmRequest.ConfirmationCode = expectedConfirmationCode;
            Assert.AreEqual(expectedConfirmationCode, accountConfirmRequest.ConfirmationCode);
        }
    }
}
