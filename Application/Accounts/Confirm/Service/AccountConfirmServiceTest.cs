﻿using NUnit.Framework;
using Moq;
using RegistryHopRestService.DAL.Application.Account.DAO;
using RegistryHopRestService.DAL.Application.Account.DTO;
using RegistryHopRestService.DAL.Application.Account.Confirm.Repository;
using RegistryHopRestService.BLL.Application.Account.Confirm.Service;
using RegistryHopRestService.BLL.Application.Account.Confirm.Model;

namespace RegistryHopRestService.BLL.Tests.Application.Accounts.Confirm.Service
{
    [TestFixture]
    public class AccountConfirmServiceTest
    {
        const string ExpectedAccountId = "d0d9b07d-63ae-41cc-976e-dcbdff1bdfb7";
        const string ExpectedName = "Neil Opet";
        const string ExpectedEmail = "yeeehaaawww@gmail.com";
        const string ExpectedCity = "Swoyersville";
        const string ExpectedState = "PA";
        const string ConfirmationCode = "8675309";

        private AccountConfirmRequest accountConfirmRequest;
        private AccountConfirmService accountConfirmService;
        private AccountConfirmRepository accountConfirmRepository;

        [SetUp]
        public void Setup()
        {
            accountConfirmRequest = new AccountConfirmRequest();
            accountConfirmRequest.ConfirmationCode = ConfirmationCode;

            var accountDAO = new Mock<IAccountDAO>();
            accountDAO.Setup(x => x.GetByConfirmationCode(It.IsAny<AccountDTO>())).Returns(new AccountDTO
                {
                    AccountId = ExpectedAccountId,
                    Name = ExpectedName,
                    Email = ExpectedEmail,
                    City = ExpectedCity,
                    State = ExpectedState
                });

            accountConfirmRepository = new AccountConfirmRepository(accountDAO.Object);

            accountConfirmService = new AccountConfirmService(accountConfirmRepository);
        }

        [Test]
        public void GivenAnAccountConfirmRequestWhenConfirmingAnAccountThenExpectANewAccount()
        {
            var actualAccount = accountConfirmService.Confirm(accountConfirmRequest);
            Assert.AreEqual(ExpectedAccountId, actualAccount.AccountId);
            Assert.AreEqual(ExpectedName, actualAccount.Name);
            Assert.AreEqual(ExpectedEmail, actualAccount.Email);
            Assert.AreEqual(ExpectedCity, actualAccount.City);
            Assert.AreEqual(ExpectedState, actualAccount.State);
        }
    }
}
