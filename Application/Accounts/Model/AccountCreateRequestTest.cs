﻿using NUnit.Framework;
using RegistryHopRestService.BLL.Application.Account.Model;

namespace RegistryHopRestService.BLL.Tests.Application.Accounts.Model
{
    [TestFixture]
    public class AccountCreateTest
    {
        [Test]
        public void GivenAnEmailWhenInstantiatingANewAccountCreateRequestThenExpectCorrectObject()
        {
            var expectedEmail = "neil.opet@gmail.com";
           
            var actualAccountCreateRequest = new AccountCreateRequest();
            actualAccountCreateRequest.Email = expectedEmail;

            Assert.AreEqual(expectedEmail, actualAccountCreateRequest.Email);
        }
    }
}