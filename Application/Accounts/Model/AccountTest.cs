﻿using NUnit.Framework;
using RegistryHopRestService.BLL.Application.Account.Model;

namespace RegistryHopRestService.BLL.Tests.Application.Accounts.Model
{
    [TestFixture]
    public class AccountTest
    {
        [Test]
        public void GivenAccountIdAndNameAndEmailAndCityAndStateWhenInstantiatingNewAccountThenExpectCorrectObject()
        {
            var expectedAccountId = "49593330-c64b-40e1-93c1-96eaa88f31c8";
            var expectedName = "Neil Opet";
            var expectedEmail = "neil.opet@gmail.com";
            var expectedCity = "Swoyersville";
            var expectedState = "PA";

            var actualAccount = new Account();
            actualAccount.AccountId = expectedAccountId;
            actualAccount.Name = expectedName;
            actualAccount.Email = expectedEmail;
            actualAccount.City = expectedCity;
            actualAccount.State = expectedState;

            Assert.AreEqual(expectedAccountId, actualAccount.AccountId);
            Assert.AreEqual(expectedName, actualAccount.Name);
            Assert.AreEqual(expectedEmail, actualAccount.Email);
            Assert.AreEqual(expectedCity, actualAccount.City);
            Assert.AreEqual(expectedState, actualAccount.State);
        }
    }
}