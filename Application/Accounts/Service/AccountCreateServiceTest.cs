﻿using NUnit.Framework;
using Moq;
using RegistryHopRestService.BLL.Application.Account.Exception;
using RegistryHopRestService.BLL.Application.Account.Service;
using RegistryHopRestService.BLL.Application.Account.Model;

namespace RegistryHopRestService.BLL.Tests.Application.Accounts.Service
{
    [TestFixture]
    public class AccountCreateServiceTest
    {
        [ExpectedException(typeof(AccountCreateException))]
        public void GivenAnInvalidEmailWhenCreatingAnAccountThenExpectAnAccountCreateException()
        {
            var accountCreateService = new AccountCreateService();
            accountCreateService.Create(new AccountCreateRequest
                {
                    Email = "test#test.com"
                });
        }
    }
}

